(org-babel-load-file "~/.emacs.d/configuration.org")

;;
;; custom
;;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ag-arguments '("--smart-case" "--stats" "--skip-vcs-ignores" "-i"))
 '(custom-safe-themes
   '("a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" default))
 '(electric-indent-mode nil)
 '(frame-background-mode 'light)
 '(linum-format "%7d ")
 '(lsp-ui-doc-border "magenta")
 '(lsp-ui-doc-delay 2)
 '(lsp-ui-doc-header t)
 '(lsp-ui-doc-include-signature t)
 '(lsp-ui-sideline-delay 2)
 '(magit-log-remove-graph-args '("--follow" "--grep" "-G" "-S" "-L" "--decorate"))
 '(magit-refs-sections-hook
   '(magit-insert-branch-description magit-insert-local-branches))
 '(magit-revision-insert-related-refs nil)
 '(mode-line-format
   '("%e" mode-line-front-space " " mode-line-buffer-identification ":" mode-line-misc-info mode-line-position mode-line-modes mode-line-end-spaces))
 '(package-selected-packages
   '(tree-sitter-langs tree-sitter ox-mediawiki org-tree-slide org-present git-commit org which-key powerline counsel-projectile rainbow-delimiters counsel desktop-environment exwm doom-modeline use-package lsp-ui lsp-mode yaml-mode graphviz-dot-mode rust-mode hmac magit org-ref highlight bind-key xah-reformat-code plantuml-mode helm flycheck-ycmd ag xcscope evil elpy column-marker))
 '(send-mail-function nil)
 '(split-height-threshold 40)
 '(split-width-threshold 100))

(put 'upcase-region 'disabled nil)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ediff-even-diff-A ((t (:background "color-255"))))
 '(ediff-even-diff-B ((t (:background "color-255"))))
 '(ediff-odd-diff-A ((t (:background "color-255"))))
 '(ediff-odd-diff-B ((t (:background "color-255"))))
 '(helm-ff-directory ((t (:extend t :foreground "DarkRed"))))
 '(helm-ff-dotted-directory ((t (:extend t :foreground "black"))))
 '(lsp-ui-doc-background ((t (:background "color-255" :foreground "black"))))
 '(lsp-ui-doc-header ((t nil)))
 '(lsp-ui-sideline-code-action ((t (:background "white" :foreground "color-233"))))
 '(magit-section-heading-selection ((t (:background "lightgoldenrod2"))))
 '(org-hide ((t (:background "brightwhite" :foreground "brightwhite"))))
 '(org-link ((t (:underline nil))))
 '(powerline-active0 ((t (:inherit mode-line :background "color-255"))))
 '(powerline-active1 ((t (:inherit mode-line :background "color-253" :foreground "black"))))
 '(powerline-active2 ((t (:inherit mode-line :background "color-251" :foreground "black"))))
 '(smblog-reqs-op-face ((t (:foreground "blue")))))

;;    ("%e" mode-line-front-space mode-line-mule-info mode-line-client mode-line-modified mode-line-remote mode-line-frame-identification mode-line-buffer-identification "   " mode-line-position "  " mode-line-modes mode-line-misc-info mode-line-end-spaces)))
