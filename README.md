slow dotfiles
=============

## How to use?

* Install [GNU stow](https://www.gnu.org/software/stow/)
* Clone this git repository
* cd dotfiles
* To setup for example emacs, call:

      stow emacs

* Repeat for each config directory
